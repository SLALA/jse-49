package ru.t1.strelcov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.AbstractEntityDTO;

import java.util.List;

public interface IDTORepository<E extends AbstractEntityDTO> {

    @NotNull
    List<E> findAll();

    void add(@NotNull final E entity);

    void update(@NotNull final E entity);

    void addAll(@NotNull final List<E> list);

    void clear();

    void remove(@NotNull final E entity);

    @Nullable
    E findById(@NotNull final String id);

    @Nullable
    E removeById(@NotNull final String id);

}
