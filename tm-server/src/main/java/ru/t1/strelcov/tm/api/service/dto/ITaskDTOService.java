package ru.t1.strelcov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTOService extends IBusinessDTOService<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllTasksByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    TaskDTO bindTaskToProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId);

    @NotNull
    TaskDTO unbindTaskFromProject(@Nullable String userId, @Nullable String taskId);

}
