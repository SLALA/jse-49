package ru.t1.strelcov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.strelcov.tm.api.endpoint.*;
import ru.t1.strelcov.tm.api.service.*;
import ru.t1.strelcov.tm.api.service.dto.IAuthDTOService;
import ru.t1.strelcov.tm.api.service.dto.IProjectDTOService;
import ru.t1.strelcov.tm.api.service.dto.ITaskDTOService;
import ru.t1.strelcov.tm.api.service.dto.IUserDTOService;
import ru.t1.strelcov.tm.component.Backup;
import ru.t1.strelcov.tm.endpoint.*;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.service.ConnectionService;
import ru.t1.strelcov.tm.service.DataService;
import ru.t1.strelcov.tm.service.LoggerService;
import ru.t1.strelcov.tm.service.PropertyService;
import ru.t1.strelcov.tm.service.dto.AuthDTOService;
import ru.t1.strelcov.tm.service.dto.ProjectDTOService;
import ru.t1.strelcov.tm.service.dto.TaskDTOService;
import ru.t1.strelcov.tm.service.dto.UserDTOService;
import ru.t1.strelcov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserDTOService userService = new UserDTOService(connectionService, propertyService);

    @NotNull
    private final IAuthDTOService authService = new AuthDTOService(userService, propertyService);

    @NotNull
    private final IDataService dataService = new DataService(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    private void initUsers() {
        userService.clear();
        userService.add("admin", "admin", Role.ADMIN);
    }

    private void initData() {
        projectService.clear();
        taskService.clear();
        initUsers();
        @Nullable String userId = userService.findByLogin("admin").getId();
        @Nullable String projectId = projectService.add(userId, "p1", "p1").getId();
        projectService.add(userId, "p2", "p2");
        taskService.add(userId, "t1", "t1").setProjectId(projectId);
        taskService.add(userId, "t2", "t2").setProjectId(projectId);
        projectService.add(userId, "pa2", "pa2");
        taskService.add(userId, "a1", "a1").setProjectId(projectId);
        taskService.add(userId, "a2", "a2").setProjectId(projectId);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initEndpoints() {
        @NotNull final Reflections reflections = new Reflections(AbstractEndpoint.class.getPackage().getName());
        @NotNull final Set<Class<? extends AbstractEndpoint>> classes = reflections.getSubTypesOf(AbstractEndpoint.class);
        classes.stream()
                .filter((clazz) -> !Modifier.isAbstract(clazz.getModifiers()))
                .forEach((clazz) -> {
                    try {
                        registerEndpoint(clazz.getDeclaredConstructor(ServiceLocator.class).newInstance(this));
                    } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                        loggerService.errors(e);
                    }
                });
    }

    public void run() {
        initJmsBroker();
        initPID();
        backup.init();
        initData();
        initEndpoints();
        displayWelcome();
    }

    public void displayWelcome() {
        loggerService.info("** TASK MANAGER SERVER STARTED **");
    }

    private void registerEndpoint(@Nullable final AbstractEndpoint endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdlURL = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdlURL);
        Endpoint.publish(wsdlURL, endpoint);
    }

    @SneakyThrows
    private void initJmsBroker() {
        BasicConfigurator.configure();
        @NotNull final BrokerService broker = new BrokerService();
        broker.addConnector("tcp://localhost:61616");
        broker.start();
    }

}
