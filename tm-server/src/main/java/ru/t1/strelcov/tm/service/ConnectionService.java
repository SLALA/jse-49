package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory getEntityManagerFactory() {
        Logger.getLogger("org.hibernate").setLevel(Level.parse(propertyService.getHibernateLogLevel()));
        return Persistence.createEntityManagerFactory("ENTERPRISE");
    }

    @Override
    public void closeFactory() {
        entityManagerFactory.close();
    }

}
