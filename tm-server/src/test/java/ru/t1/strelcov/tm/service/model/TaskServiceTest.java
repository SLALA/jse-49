package ru.t1.strelcov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.model.ITaskService;
import ru.t1.strelcov.tm.api.service.model.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.AbstractException;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.service.ConnectionService;
import ru.t1.strelcov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final ITaskService service = new TaskService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private static final User testUser = new User("test2", "test", Role.USER);

    @NotNull
    private static final User testUserUnique = new User("testUnique", "test", Role.USER);

    @NotNull
    private static final String testUserId = testUser.getId();

    @NotNull
    private static final String testUserUniqueId = testUserUnique.getId();

    @NotNull
    private static final String NAME_UNIQUE = "NAME_UNIQUE";

    @NotNull
    final List<Task> tasks = Arrays.asList(
            new Task(testUser, "pr1"),
            new Task(testUser, "pr4"),
            new Task(testUser, "pr3"));

    @Before
    public void before() {
        try {
            @NotNull User existedUser = userService.findByLogin(testUser.getLogin());
            userService.removeByLogin(existedUser.getLogin());
            service.clear(existedUser.getId());
        } catch (@NotNull Exception ignored) {
        }
        try {
            @NotNull User existedUser = userService.findByLogin(testUserUnique.getLogin());
            userService.removeByLogin(existedUser.getLogin());
            service.clear(existedUser.getId());
        } catch (@NotNull Exception ignored) {
        }
        userService.add(testUser);
        userService.add(testUserUnique);
        service.addAll(tasks);
    }

    @After
    public void after() {
        service.clear(testUserId);
        service.clear(testUserUniqueId);
        userService.removeById(testUserId);
        userService.removeById(testUserUniqueId);
    }

    @AfterClass
    public static void afterClass() {
        connectionService.closeFactory();
    }

    @Test
    public void addTest() {
        @NotNull final Task task = new Task(testUser, "pr1");
        int size = service.findAll(testUserId).size();
        service.add(null);
        Assert.assertEquals(size, service.findAll(testUserId).size());
        service.add(task);
        Assert.assertEquals(size + 1, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).contains(task));
    }

    @Test
    public void addAllTest() {
        @NotNull final Task[] newTasks = new Task[]{new Task(testUser, "pr11"), new Task(testUser, "pr22")};
        @NotNull final ArrayList<Task> list = new ArrayList<>(Arrays.asList(newTasks));
        int size = service.findAll(testUserId).size();
        int listSize = list.size();
        @NotNull final ArrayList<Task> listWithNulls = new ArrayList<>(list);
        listWithNulls.addAll(Arrays.asList(null, null));
        service.addAll(listWithNulls);
        Assert.assertEquals(size + listSize, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).containsAll(list));
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId"));
        for (@NotNull final Task task : service.findAll(testUserId)) {
            Assert.assertTrue(service.findAll(testUserId).contains(service.findById(task.getId())));
            Assert.assertEquals(task, service.findById(task.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId"));
        @NotNull final Task task = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeById(task.getId()));
        Assert.assertFalse(service.findAll(testUserId).contains(task));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void updateByIdByUserIdTest() {
        for (@NotNull final Task task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUser().getId();
            @NotNull final String id = task.getId();
            Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, null, "newName", "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateById(null, id, "newName", "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateById(userId, id, "", "newDesc"));
            service.updateById(userId, id, "newName", "newDesc");
            Assert.assertEquals("newName", service.findById(userId, id).getName());
            Assert.assertEquals("newDesc", service.findById(userId, id).getDescription());
            Assert.assertNotNull(service.updateById(userId, id, "newName", null));
            Assert.assertNull(service.findById(userId, id).getDescription());
        }
    }

    @Test
    public void updateByNameByUserIdTest() {
        for (@NotNull final Task task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUser().getId();
            @NotNull final String name = task.getName();
            @Nullable final String newName = "newName";
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(userId, null, newName, "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(null, name, newName, "newDesc"));
            Assert.assertThrows(AbstractException.class, () -> service.updateByName(userId, name, null, "newDesc"));
            service.updateByName(userId, name, newName, "newDesc");
            Assert.assertEquals("newDesc", service.findByName(userId, newName).getDescription());
            Assert.assertEquals(newName, service.findById(task.getId()).getName());
        }
    }

    @Test
    public void changeStatusByIdByUserIdTest() {
        for (@NotNull final Task task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUser().getId();
            @NotNull final String id = task.getId();
            for (@NotNull final Status status : Status.values()) {
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusById(null, id, status));
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusById(userId, null, status));
                service.changeStatusById(userId, id, status);
                Assert.assertEquals(status, service.findById(userId, id).getStatus());
            }
        }
    }

    @Test
    public void changeStatusByNameByUserIdTest() {
        for (@NotNull final Task task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUser().getId();
            @NotNull final String name = task.getName();
            for (@NotNull final Status status : Status.values()) {
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(null, name, status));
                Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, status));
                service.changeStatusByName(userId, name, status);
                Assert.assertEquals(status, service.findByName(userId, name).getStatus());
            }
            Assert.assertThrows(AbstractException.class, () -> service.changeStatusByName(userId, null, Status.IN_PROGRESS));
        }
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final Task[] newTasks = new Task[]{new Task(testUserUnique, "pr2"), new Task(testUserUnique, "pr1")};
        Assert.assertThrows(AbstractException.class, () -> service.findAll(null));
        @NotNull final ArrayList<Task> list = new ArrayList<>(Arrays.asList(newTasks));
        @NotNull final String userId = list.get(0).getUser().getId();
        Assert.assertEquals(0, service.findAll(userId).size());
        service.addAll(list);
        Assert.assertEquals(list.size(), service.findAll(userId).size());
        Assert.assertTrue(list.containsAll(service.findAll(userId)));
    }

    @Test
    public void findAllSortedByUserIdTest() {
        @NotNull final Task[] newTasks = new Task[]{new Task(testUserUnique, "pr2"), new Task(testUserUnique, "pr1")};
        @NotNull final ArrayList<Task> list = new ArrayList<>(Arrays.asList(newTasks));
        @NotNull Comparator<Task> comparator = SortType.NAME.getComparator();
        @NotNull String sort = SortType.NAME.name();
        @Nullable String userId = list.get(0).getUser().getId();
        Assert.assertThrows(AbstractException.class, () -> service.findAll(null, sort));
        Assert.assertEquals(0, service.findAll(userId).size());
        service.addAll(list);
        @NotNull final List<Task> sortedList = service.findAll(userId, sort);
        Assert.assertEquals(list.size(), sortedList.size());
        Assert.assertTrue(list.containsAll(sortedList));
        Assert.assertEquals(sortedList.stream().sorted(comparator)
                .collect(Collectors.toList()), sortedList);
    }

    @Test
    public void findByNameByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findByName(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", "notExistedId"));
        for (@NotNull final Task task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUser().getId();
            @NotNull final String name = task.getName();
            Assert.assertTrue(service.findAll(testUserId).contains(service.findByName(userId, name)));
            Assert.assertEquals(task.getName(), service.findByName(userId, name).getName());
            Assert.assertThrows(AbstractException.class, () -> service.findByName("notExistedId", name));
            Assert.assertThrows(AbstractException.class, () -> service.findByName(userId, "notExistedId"));
        }
    }

    @Test
    public void removeByNameByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeByName(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeByName("notExistedId", null));
        @NotNull final Task task = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeByName(task.getUser().getId(), task.getName()));
        Assert.assertFalse(service.findAll(testUserId).contains(task));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void findByIdByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", "notExistedId"));
        for (@NotNull final Task task : service.findAll(testUserId)) {
            @NotNull final String userId = task.getUser().getId();
            Assert.assertTrue(service.findAll(testUserId).contains(service.findById(userId, task.getId())));
            Assert.assertEquals(task, service.findById(userId, task.getId()));
            Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId", task.getId()));
            Assert.assertThrows(AbstractException.class, () -> service.findById(userId, "notExistedId"));
        }
    }

    @Test
    public void removeByIdByUserIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId", null));
        @NotNull final Task task = service.findAll(testUserId).get(0);
        int fullSize = service.findAll(testUserId).size();
        Assert.assertNotNull(service.removeById(task.getUser().getId(), task.getId()));
        Assert.assertFalse(service.findAll(testUserId).contains(task));
        Assert.assertEquals(fullSize - 1, service.findAll(testUserId).size());
    }

    @Test
    public void clearByUserIdTest() {
        final String userId = testUserId;
        Assert.assertThrows(AbstractException.class, () -> service.clear(null));
        @NotNull final List<Task> list = service.findAll(userId);
        int size = list.size();
        int fullSize = service.findAll().size();
        service.clear(userId);
        service.clear("notExistedId");
        Assert.assertEquals(0, service.findAll(userId).size());
        Assert.assertEquals(fullSize - size, service.findAll().size());
    }

    @Test
    public void createTest() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, "notExistedId", "descr_new"));
        Assert.assertThrows(AbstractException.class, () -> service.add("notExistedId", null, "descr_new"));
        int size = service.findAll(testUserId).size();
        Assert.assertNotNull(service.add(testUserId, NAME_UNIQUE, "descr_new"));
        Assert.assertEquals(size + 1, service.findAll(testUserId).size());
        Assert.assertTrue(service.findAll(testUserId).contains(service.findByName(testUserId, NAME_UNIQUE)));
    }

}
