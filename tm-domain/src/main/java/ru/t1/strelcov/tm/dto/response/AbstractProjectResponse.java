package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResultResponse {

    @NotNull
    private ProjectDTO project;

    public AbstractProjectResponse(@NotNull final ProjectDTO project) {
        this.project = project;
    }

}
