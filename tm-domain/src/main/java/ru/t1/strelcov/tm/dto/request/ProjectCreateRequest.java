package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class ProjectCreateRequest extends AbstractUserRequest {

    @Nullable
    private String description;

    @Nullable
    private String name;

    public ProjectCreateRequest(@Nullable final String token, @Nullable final String name, @Nullable final String description) {
        super(token);
        this.name = name;
        this.description = description;
    }

}
