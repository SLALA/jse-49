package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class UserRegisterRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String role;

    public UserRegisterRequest(@Nullable final String token, @Nullable final String login, @Nullable final String password, @Nullable final String role) {
        super(token);
        this.login = login;
        this.password = password;
        this.role = role;
    }

}
