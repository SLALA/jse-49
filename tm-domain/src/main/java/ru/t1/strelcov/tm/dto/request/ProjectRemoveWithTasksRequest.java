package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class ProjectRemoveWithTasksRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectRemoveWithTasksRequest(@Nullable final String token, @Nullable final String id) {
        super(token);
        this.id = id;
    }

}
