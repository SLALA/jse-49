package ru.t1.strelcov.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.entity.IHasName;

import java.util.Comparator;

public final class ComparatorByName implements Comparator<IHasName> {

    private static final ComparatorByName INSTANCE = new ComparatorByName();

    private ComparatorByName() {
    }

    public static ComparatorByName getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasName o1, @Nullable final IHasName o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
