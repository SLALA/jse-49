package ru.t1.strelcov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataXmlJAXBLoadRequest extends AbstractUserRequest {

    public DataXmlJAXBLoadRequest(@Nullable final String token) {
        super(token);
    }

}
