package ru.t1.strelcov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String MONGO_HOST = "localhost";

    private static final int MONGO_PORT = 27017;

    @NotNull
    private static final String MONGO_DB_NAME = "tm_log";

    @NotNull final MongoClient client = new MongoClient(MONGO_HOST, MONGO_PORT);

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final String json) {
        Logger.getRootLogger().info(json);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Map map = objectMapper.readValue(json, LinkedHashMap.class);
        @NotNull final String collectionName = map.get("table").toString();
        @NotNull final MongoDatabase database = client.getDatabase(MONGO_DB_NAME);
        if (database.getCollection(collectionName) == null) database.createCollection(collectionName);
        @NotNull final MongoCollection<Document> collection = database.getCollection(collectionName);
        @NotNull final Document document = new Document(map);
        collection.insertOne(document);
    }

}
