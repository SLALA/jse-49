package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.dto.request.TaskUpdateByIdRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NEW NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        @NotNull final TaskDTO task = taskEndpoint.updateByIdTask(new TaskUpdateByIdRequest(getToken(), id, name, description)).getTask();
        showTask(task);
    }

}
