package ru.t1.strelcov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.bootstrap.Bootstrap;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class FileScanner implements Runnable {

    @NotNull
    private final static Integer INTERVAL = 5;

    @NotNull
    private final static String PATH = "./commands";

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> commands = new ArrayList<>();

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void run() {
        @NotNull File directory = new File(PATH);
        Optional.ofNullable(directory.listFiles()).map(Arrays::stream).orElseGet(Stream::empty)
                .filter(File::isFile).filter((item) -> commands.contains(item.getName()))
                .sorted(Comparator.comparing((item) -> {
                    try {
                        return Files.readAttributes(item.toPath(), BasicFileAttributes.class).creationTime();
                    } catch (Exception e) {
                        bootstrap.getLoggerService().info("File scanner read attribute error: " + e.getMessage());
                        return FileTime.from(Instant.EPOCH);
                    }
                }))
                .forEach((item) -> {
                    try {
                        System.out.println("[FILE SCANNER START]");
                        bootstrap.parseCommand(item.getName());
                    } catch (Exception e) {
                        bootstrap.getLoggerService().info("File scanner parse command error: " + e.getMessage());
                    } finally {
                        item.delete();
                        System.out.println("[FILE SCANNER END]");
                    }
                });
    }

    public void init() {
        bootstrap.getCommandService().getArguments().forEach((item) -> commands.add(item.name()));
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

}
