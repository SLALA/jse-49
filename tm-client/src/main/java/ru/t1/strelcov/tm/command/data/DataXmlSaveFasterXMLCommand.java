package ru.t1.strelcov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.request.DataXmlFasterXMLSaveRequest;
import ru.t1.strelcov.tm.enumerated.Role;

public class DataXmlSaveFasterXMLCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-xml-save-fasterxml";
    }

    @Override
    @NotNull
    public String description() {
        return "Save entities data to xml file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML SAVE]");
        serviceLocator.getDataEndpoint().saveXmlFasterXMLData(new DataXmlFasterXMLSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
