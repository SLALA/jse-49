package ru.t1.strelcov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.dto.request.ServerAboutRequest;

public final class DisplayAboutServerCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "server-about";
    }

    @NotNull
    @Override
    public String description() {
        return "Display server developer info.";
    }

    @Override
    public void execute() {
        @NotNull final ISystemEndpoint systemEndpoint = serviceLocator.getSystemEndpoint();
        System.out.println("[ABOUT SERVER]");
        System.out.println(systemEndpoint.getAbout(new ServerAboutRequest()).getName());
        System.out.println(systemEndpoint.getAbout(new ServerAboutRequest()).getEmail());
    }

}
