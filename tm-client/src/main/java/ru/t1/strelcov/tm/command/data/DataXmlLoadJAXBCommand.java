package ru.t1.strelcov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.request.DataXmlJAXBLoadRequest;
import ru.t1.strelcov.tm.enumerated.Role;

public class DataXmlLoadJAXBCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-xml-load-jaxb";
    }

    @Override
    @NotNull
    public String description() {
        return "Load entities data from xml file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML LOAD]");
        serviceLocator.getDataEndpoint().loadXmlJAXBData(new DataXmlJAXBLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
