package ru.t1.strelcov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.strelcov.tm.api.endpoint.*;
import ru.t1.strelcov.tm.api.repository.ICommandRepository;
import ru.t1.strelcov.tm.api.service.*;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.component.FileScanner;
import ru.t1.strelcov.tm.repository.CommandRepository;
import ru.t1.strelcov.tm.service.CommandService;
import ru.t1.strelcov.tm.service.LoggerService;
import ru.t1.strelcov.tm.service.PropertyService;
import ru.t1.strelcov.tm.service.TokenService;
import ru.t1.strelcov.tm.util.SystemUtil;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Set;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final Integer port = propertyService.getServerPort();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance(host, port);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(host, port);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);

    @NotNull
    private final IDataEndpoint dataEndpoint = IDataEndpoint.newInstance(host, port);

    @NotNull
    private final ITokenService tokenService = new TokenService();

    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections(AbstractCommand.class.getPackage().getName());
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        classes.stream()
                .filter((clazz) -> !Modifier.isAbstract(clazz.getModifiers()))
                .sorted(Comparator.comparing(Class::getName))
                .forEach((clazz) -> {
                    try {
                        register(clazz.newInstance());
                    } catch (InstantiationException | IllegalAccessException e) {
                        loggerService.errors(e);
                    }
                });
    }

    public void register(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void run(@Nullable String... args) {
        initPID();
        initCommands();
        displayWelcome();
        fileScanner.init();
        try {
            if (parseArgs(args))
                System.exit(0);
        } catch (Exception e) {
            loggerService.errors(e);
            System.exit(0);
        }
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                parseCommand(TerminalUtil.nextLine());
                System.out.println("[OK]");
            } catch (Throwable e) {
                loggerService.errors(e);
                System.err.println("[FAIL]");
            } finally {
                System.out.println();
            }
        }
    }

    public void displayWelcome() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        return true;
    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null) return;
        loggerService.commands(arg);
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByArg(arg);
        abstractCommand.execute();
    }

    public void parseCommand(@Nullable final String command) {
        if (command == null) return;
        loggerService.commands(command);
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        abstractCommand.execute();
    }

}
